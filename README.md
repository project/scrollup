# Scroll Up

This module allows users with appropriate permissions to add and 
configure "Scroll up" button. Button allows users to scroll up to 
the top of the page.

Scroll up uses vanilla JavaScript for smooth page scrolling.

## Configuration (`admin/config/system/scrollup`):

* display scroll button on specific themes.
* positioning scroll button on bottom of the page.
* speed of the scrolling.
* amount of scroll after which scroll button should appear.
* background color and hover of the scroll button.

## Maintainers

- Gajendra Sharma - [gajendra.sharma](https://www.drupal.org/u/gajendrasharma)
- Vladimir Roudakov - [VladimirAus](https://www.drupal.org/u/vladimiraus)
- Himmat Bhatia - [himmatbhatia](https://www.drupal.org/u/himmatbhatia)
