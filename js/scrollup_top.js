/**
 * @file
 * ScrollUp javascript file.
 */

(function (Drupal, drupalSettings, once) {
  Drupal.behaviors.scrollup = {
    attach() {
      let linkTitle = 'Scroll to the top of the page.';
      let linkContent = '';
      if (
        drupalSettings.scrollup_title !== '' &&
        drupalSettings.scrollup_title !== null
      ) {
        linkTitle = drupalSettings.scrollup_title;
        linkContent = drupalSettings.scrollup_title;
      }

      const bodyContainer = once('scrollup', document.querySelector('body'));

      if (bodyContainer.length === 0) {
        return;
      }

      const [body] = bodyContainer;
      body.insertAdjacentHTML(
        'beforeend',
        `<a href="#" title="${linkTitle}" class="scrollup">Scroll<span class="scroll-title">${linkContent}</span></a>`,
      );

      const scrollUpButton = document.querySelector('.scrollup');
      const position = drupalSettings.scrollup_position;
      const buttonBgColor = drupalSettings.scrollup_button_bg_color;
      const hoverButtonBgColor = drupalSettings.scrollup_button_hover_bg_color;
      const scrollWindowPosition = parseInt(
        drupalSettings.scrollup_window_position,
        10,
      );
      const scrollSpeed = parseInt(drupalSettings.scrollup_speed, 10);

      if (position === 1) {
        if (document.dir === 'ltr') {
          scrollUpButton.style.right = '0px';
        } else {
          scrollUpButton.style.left = '0px';
        }
      } else {
        scrollUpButton.style.left = '0px';
      }
      scrollUpButton.style.backgroundColor = `${buttonBgColor}`;

      scrollUpButton.addEventListener('mouseover', function (event) {
        event.preventDefault();
        scrollUpButton.style.backgroundColor = `${hoverButtonBgColor}`;
      });

      scrollUpButton.addEventListener('mouseleave', function (event) {
        event.preventDefault();
        scrollUpButton.style.backgroundColor = `${buttonBgColor}`;
      });

      window.addEventListener('scroll', function (event) {
        event.preventDefault();
        scrollUpButton.style.display =
          window.pageYOffset > scrollWindowPosition ? 'block' : 'none';
      });

      scrollUpButton.addEventListener('click', function (event) {
        event.preventDefault();
        document.querySelectorAll('html, body').forEach((node) =>
          node.scrollTo({
            top: 0,
            behavior: 'smooth',
            duration: scrollSpeed,
          }),
        );
      });
    },
  };
})(Drupal, drupalSettings, once);
